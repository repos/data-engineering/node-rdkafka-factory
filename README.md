# node-rdkafka-factory

This module contains slim Promise wrappers around [node-rdkafka](https://github.com/Blizzard/node-rdkafka) client classes.

Currently, only the [Standard Producer API](https://github.com/Blizzard/node-rdkafka#standard-api) is wrapped.

# Usage

```javascript

const kafka = require('node-rdkafka-factory');

const rdkafka_conf = {
    'metadata.broker.list': 'localhost:9092'
};
const rdkafka_topic_conf = {};

// Create a 'GuaranteedProducer'.
const guaranteedKafkaProducer = await kafka.GuaranteedProducer.factory(
    rdkafka_conf,
    rdkafka_topic_conf
);
// This will wait for the Kafka producer to ACK before the async Promise resolves.
await guaranteedKafkaProducer.produce('mytopic', undefined, Buffer.from('my message'), undefined);


// Create a 'HastyProducer'.
const hastyKafkaProducer = await kafka.HastyProducer.factory(
    rdkafka_conf,
    rdkafka_topic_conf
);

// This will resolve as soon as node-rdkafka queues the produce request interally.
// It will not wait for the Kafka broker to receive the produce request or ACK it.
await hastyKafkaProducer.produce('mytopic', undefined, Buffer.from('my message'), undefined);
```
